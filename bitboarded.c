// bitboarded: bitboard editor

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#define SET(b,i) (b |= (uint64_t)1 << (i))
#define UNSET(b,i) (b &= ~((uint64_t)1 << (i)))
#define ISSET(b,i) ((b) >> (i) & 1)

uint64_t bb;

void
prompt()
{
	puts("           (black)");
	for (int r = 8; r--;) {
		printf(" %d ", r+1);
		for (int f = 0; f < 8; f++) {
			if (r+f&1)
				printf("\033[30;47m");
			else
				printf("\033[30;46m");
			unsigned i = 8*r+f;
			printf(" %u ", (unsigned)ISSET(bb, i));
		}
		printf("\033[39;49m\n");
	}
	printf("    a  b  c  d  e  f  g  h\n");
	puts("           (white)");
	printf("> ");
}

int
main()
{
	char l[999], *p;
	while (prompt(), fgets(l, 999, stdin)) {
		switch (l[0]) {
		case 0: break;
		case 'l': // load
			bb = strtoull(l+1, NULL, 0);
			break;
		case 'c': // hexadecimal code
			printf("0x%" PRIx64 "\n", bb);
			break;
		case 'd': // decimal code
			printf("%" PRId64 "\n", bb);
			break;
		case 'o': // octal code
			printf("0%" PRIo64 "\n", bb);
			break;
		case '!': case '*': // set/unset positions
			p = l + 1;
			while (*p && *p != '\n') {
				int i = (p[0]-'a') + 8*(p[1]-'1');
				if (l[0] == '!')
					SET(bb, i);
				else
					UNSET(bb, i);
				p += 2;
			}
			break;
		case 'q': // quit
			return 0;
		default:
			printf("?\n");
		}
		printf("\n");
		fflush(stdout);
	}
}
