// blunderbuss: Thomas Oltmann's stupid chess engine

#include <stdio.h>
#include <stdint.h>
#include <string.h>
//#include <strings.h>
#include <limits.h>
#include <sys/time.h>
#include <setjmp.h>

// whether a given position i is occupied by one of my pieces
#define ME(i) (board[i] && ((board[i] >> 5) & 1) == me)
// whether a given position i is occupied by an enemy piece
#define EN(i) (board[i] && ((board[i] >> 5) & 1) != me)

// Set bit in bitvector / bitboard
#define SET(b,i) (b |= (uint64_t)1 << (i))

/* Format of supplementary state variable bits:
 *  0- 7: Passant. 1 If white moved pawn twice last move. One bit per file.
 *     8: 1 if white queenside rook has moved at some point.
 *     9: 1 if white king has moved at some point.
 *    10: 1 if white kingside rook has moved at some point.
 * 16-23: Passant. 1 If black moved pawn twice last move. One bit per file.
 *    24: 1 if black queenside rook has moved at some point.
 *    25: 1 if black king has moved at some point.
 *    26: 1 if black kingside rook has moved at some point.
 */

char board[64];
unsigned supp;
int me;

void
reset(void)
{
	memcpy(board, "RNBQKBNRPPPPPPPP", 16);
	memset(board+16, 0, 4*8);
	memcpy(board+6*8, "pppppppprnbqkbnr", 16);
	supp = me = 0;
}

unsigned
move(unsigned m)
{
	int s = m&0xFF;
	int e = (m>>8)&0xFF;
	char p = board[s];
	unsigned c = board[e]; // capture, if any

	// clear passant from previous move
	supp &= ~(0xFF << 16*me);

	if (p == 'K' || p == 'k') {
		supp |= 2 << 8+16*me;
	}
	if ((p == 'R' || p == 'r') && (s&7) == 0) {
		supp |= 1 << 8+16*me;
	}
	if ((p == 'R' || p == 'r') && (s&7) == 7) {
		supp |= 3 << 8+16*me;
	}
	if (p == 'P' && e == s + 16) {
		supp |= 1 << (s&7)+16*me;
	}
	if (p == 'p' && e == s - 16) {
		supp |= 1 << (s&7)+16*me;
	}
	
	if (p == 'P' && e >= 7*8) { // white promotion
		p = m >> 16;
	} else if (p == 'p' && e < 8) { // black promotion
		p = (m >> 16) | 0x20;
	} else if (p == 'P' && ((e-s)&7) && !c) { // white en passant
		c = board[e-8];
		board[e-8] = 0;
	} else if (p == 'p' && ((e-s)&7) && !c) { // black en passant
		c = board[e+8];
		board[e+8] = 0;
	} else if ((p == 'K' || p == 'k') && e == s - 2) { // castling queenside
		board[s-1] = board[s-4];
		board[s-4] = 0;
	} else if ((p == 'K' || p == 'k') && e == s + 2) { // castling kingside
		board[s+1] = board[s+3];
		board[s+3] = 0;
	}

	board[s] = 0;
	board[e] = p;

	return c;
}

void
evom(unsigned m, unsigned c)
{
	int s = m&0xFF;
	int e = (m>>8)&0xFF;
	char p = board[e];

	if (m >> 16) { // promotion
		p = 'P' & 0x20 * me;
	} else if (p == 'P' && ((e-s)&7) && !board[e]) { // white en passant
		board[e-8] = c;
		c = 0;
	} else if (p == 'p' && ((e-s)&7) && !board[e]) { // black en passant
		board[e+8] = c;
		c = 0;
	} else if ((p == 'K' || p == 'k') && e == s - 2) { // castling queenside
		board[s-4] = board[s-1];
		board[s-1] = 0;
	} else if ((p == 'K' || p == 'k') && e == s + 2) { // castling kingside
		board[s+3] = board[s+1];
		board[s+1] = 0;
	}

	board[e] = c;
	board[s] = p;
}

uint64_t
preach(int r, int f, int i)
{
	uint64_t b = 0;
	int d = 8-16*me;
	if (!board[i + d]) {
		SET(b, i + d);
	}
	if (r == (me ? 6 : 1) && !board[i + d] && !board[i + 2*d]) { // two spaces
		SET(b, i + 2*d);
	}
	// capture
	if (f != 0 && EN(i + d - 1)) {
		SET(b, i + d - 1);
	}
	if (f != 7 && EN(i + d + 1)) {
		SET(b, i + d + 1);
	}
	// TODO fix en passant
#if 0
	// en passant
	if (f != 0 && !board[i + d - 1] && ((passant[!me] >> (i + d - 1)) & 1)) {
		SET(b, i + d - 1);
	}
	if (f != 7 && !board[i + d + 1] && ((passant[!me] >> (i + d + 1)) & 1)) {
		SET(b, i + d + 1);
	}
#endif
	return b;
}

uint64_t
lreach(int r, int f, int d, unsigned rp, unsigned fp)
{
	uint64_t b = 0;
	while (d--) {
		int rd = (rp >> 3*d & 7) - 1;
		int fd = (fp >> 3*d & 7) - 1;
		for (int l = 1;; l++) {
			unsigned rx = r + rd * l;
			if (rx > 7) break;
			unsigned fx = f + fd * l;
			if (fx > 7) break;
			unsigned ix = 8*rx+fx;
			if (ME(ix)) break;
			SET(b, ix);
			if (EN(ix)) break;
		}
	}
	return b;
}

uint64_t
jreach(int r, int f, int d, unsigned rp, unsigned fp)
{
	uint64_t b = 0;
	while (d--) {
		unsigned rx = r + (rp >> 3*d & 7) - 2;
		if (rx > 7) continue;
		unsigned fx = f + (fp >> 3*d & 7) - 2;
		if (fx > 7) continue;
		unsigned ix = 8*rx+fx;
		if (ME(ix)) continue;
		SET(b, ix);
	}
	return b;
}

uint64_t
reach(int i)
{
	char p = board[i];
	int r = i/8;
	int f = i&7;
	uint64_t b = 0;
	switch (p & 0xDF) {
	case 'P':
		b = preach(r, f, i);
		break;
	case 'B':
		b = lreach(r, f, 4, 00022, 00220);
		break;
	case 'R':
		b = lreach(r, f, 4, 00121, 01012);
		break;
	case 'Q':
		b = lreach(r, f, 8, 000220121, 002201012);
		break;
	case 'N':
		b = jreach(r, f, 8, 034101430, 041034301);
		break;
	case 'K':
		b = jreach(r, f, 8, 012313123, 011122333);
		// castling
		// TODO don't go through any checked tiles
		if ((supp >> 8+16*me & 3) == 0 && !board[i - 1] && !board[i - 2] && !board[i - 3] && board[i - 4] == ('R' & 0x20*me)) {
			SET(b, i - 2);
		}
		if ((supp >> 8+16*me & 6) == 0 && !board[i + 1] && !board[i + 2] && board[i + 3] == ('R' & 0x20*me)) {
			SET(b, i + 2);
		}
		break;
	}
	return b;
}

unsigned
genpseudo(unsigned *moves)
{
	unsigned count = 0;
	for (unsigned i = 0; i < 64; i++) {
		if (!ME(i)) continue;
		uint64_t b = reach(i);
		if (!b) continue;
		for (unsigned j = 0; j < 64; j++) {
			if ((b >> j) & 1) {
				// construct move
				unsigned m = j<<8 | i;
				if (board[i] == 'P' && j/8 == 7) {
					m |= 'Q' << 16;
				}
				if (board[i] == 'p' && j/8 == 0) {
					m |= 'Q' << 16;
				}
				moves[count++] = m;
			}
		}
	}
	return count;
}

long
pieceval(char p)
{
	switch (p) {
	case 'P': case 'p': return 100;
	case 'B': case 'b': return 300;
	case 'N': case 'n': return 300;
	case 'R': case 'r': return 500;
	case 'Q': case 'q': return 900;
	case 'K': case 'k': return 99999;
	default: return 0;
	}
}

long
eval()
{
	long score = 0;
	for (unsigned i = 0; i < 64; i++) {
		if (ME(i)) {
			score += pieceval(board[i]);
			if (!(board[i] & 0x20)) {
				score += 5 * (i/8) * (board[i] == 'P' ? 4 : 1);
			} else {
				score += 5 * (7-i/8) * (board[i] == 'p' ? 4 : 1);
			}
		}
		if (EN(i)) {
			score -= pieceval(board[i]);
			if (!(board[i] & 0x20)) {
				score -= 5 * (i/8) * (board[i] == 'P' ? 4 : 1);
			} else {
				score -= 5 * (7-i/8) * (board[i] == 'p' ? 4 : 1);
			}
		}
	}
	return score;
}

struct timeval since;
jmp_buf endjmp;

long maxsearch(int, long, long, unsigned *);

long
minsearch(int depth, long alpha, long beta, unsigned *bestseq)
{
	struct timeval now;
	gettimeofday(&now, 0);
	int64_t d1 = now.tv_sec - since.tv_sec;
	d1 *= 1000;
	int64_t d2 = now.tv_usec - since.tv_usec;
	d2 /= 1000;
	if (d1 + d2 >= 100) longjmp(endjmp, 1);

	if (!depth) return -eval();
	unsigned osupp = supp;
	unsigned moves[256], count;
	count = genpseudo(moves);
	for (unsigned i = 0; i < count; i++) {
		unsigned seq[depth];
		seq[0] = moves[i];
		// make & score move
		unsigned c = move(moves[i]);
		me = !me;
		long score = maxsearch(depth-1, alpha, beta, seq+1);
		me = !me;
		evom(moves[i], c);
		supp = osupp;

		if (score <= alpha)
			return alpha;

		if (score < beta) {
			beta = score;
			memcpy(bestseq, seq, depth * sizeof *seq);
		}
	}
	return beta;
}

long
maxsearch(int depth, long alpha, long beta, unsigned *bestseq)
{
	struct timeval now;
	gettimeofday(&now, 0);
	int64_t d1 = now.tv_sec - since.tv_sec;
	d1 *= 1000;
	int64_t d2 = now.tv_usec - since.tv_usec;
	d2 /= 1000;
	if (d1 + d2 >= 100) longjmp(endjmp, 1);


	if (!depth) return eval();
	unsigned osupp = supp;
	unsigned moves[256], count;
	count = genpseudo(moves);
	for (unsigned i = 0; i < count; i++) {
		unsigned seq[depth];
		seq[0] = moves[i];
		// make & score move
		unsigned c = move(moves[i]);
		me = !me;
		long score = minsearch(depth-1, alpha, beta, seq+1);
		me = !me;
		evom(moves[i], c);
		supp = osupp;

		if (score >= beta)
			return beta;

		if (score > alpha) {
			alpha = score;
			memcpy(bestseq, seq, depth * sizeof *seq);
		}
	}
	return alpha;
}

void
go()
{
	unsigned bestmove = 0, bestseq[30];
	if (!setjmp(endjmp)) {
		gettimeofday(&since, 0);
		for (int depth = 1; depth < 30; depth++) {
			printf("info depth %d\n", depth);
			maxsearch(depth, LONG_MIN, LONG_MAX, bestseq);
			bestmove = bestseq[0];
		}
	} else {
		if (bestmove) {
			printf("bestmove %c%c%c%c",
				(bestmove&7)+'a', (bestmove/8&7)+'1',
				(bestmove>>8&7)+'a', ((bestmove>>8)/8&7)+'1');
			if (bestmove >> 16)
				putchar(bestmove >> 16);
			putchar('\n');
		} else {
			puts("bestmove 0000");
		}
	}
}

#if FULL
void
show()
{
	for (int r = 0; r < 8; r++) {
		for (int f = 0; f < 8; f++) {
			char p = board[f+8*r];
			if (!p) p = '.';
			putchar(p);
		}
		putchar('\n');
	}
}

uint64_t
perft(int depth)
{
	if (depth == 0)
		return 1;

	uint64_t total = 0;
	unsigned myMoves[256], myCount;
	myCount = genpseudo(myMoves);
	unsigned osupp = supp;
	for (unsigned i = 0; i < myCount; i++) {
		unsigned c = move(myMoves[i]);
		me = !me;

		int legal = 1;
		unsigned enMoves[256], enCount;
		enCount = genpseudo(enMoves);
		unsigned osupp2 = supp;
		for (unsigned j = 0; j < enCount && legal; j++) {
			unsigned c2 = move(enMoves[j]);
			if (c2 == 'K' || c2 == 'k') {
				legal = 0;
			}
			evom(enMoves[j], c2);
			supp = osupp2;
		}

		if (legal) {
			total += perft(depth - 1);
		}

		me = !me;
		evom(myMoves[i], c);
		supp = osupp;
	}
	return total;
}
#endif

int
main()
{
	char l[999], *t;
	while (fgets(l, 999, stdin)) {
		t = strtok(l, " \t\n");
		if (!t) continue;
		if (!strcmp(t, "go")) {
			go();
		} else if (!strcmp(t, "isready")) {
			puts("readyok");
		} else if (!strcmp(t, "position")) {
			t = strtok(0, " \t\n");
			t = strtok(0, " \t\n");
			reset();
			while ((t = strtok(0, " \t\n"))) {
				unsigned m = 0;
				m |= t[0]-'a';
				m |= (t[1]-'1')*8;
				m |= (t[2]-'a')<<8;
				m |= ((t[3]-'1')*8)<<8;
				m |= (t[4]&~0x20)<<16;
				move(m);
				me = !me;
			}
#if FULL
		} else if (!strcmp(t, "show")) {
			show();
		} else if (!strcmp(t, "perft")) {
			t = strtok(0, " \t\n");
			uint64_t total = perft(*t - '0');
			printf("perftresult %lu\n", total);
#endif
		} else if (!strcmp(t, "uci")) {
			puts("id name bb_deepen\nid author Thomas Oltmann\nuciok");
		} else if (!strcmp(t, "quit")) {
			return 0;
		}
		fflush(stdout);
	}
}
