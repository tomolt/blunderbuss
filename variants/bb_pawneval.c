// blunderbuss: Thomas Oltmann's stupid chess engine

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <sys/time.h>
#include <setjmp.h>

#include <assert.h>

#define ABS(x) ((x)<0?-(x):(x))
// Set bit in bitvector / bitboard
#define SET(b,i) (b |= (uint64_t)1 << (i))
#define UNSET(b,i) (b &= ~((uint64_t)1 << (i)))
#define ISSET(b,i) ((b) >> (i) & 1)
#define GOOD ((int64_t)1<<60) // don't use INT64_(MIN|MAX), overflow may occur when flipping the sign!

/* compact encoding for pieces:
 * 0: pawn
 * 1: knight
 * 2: bishop
 * 3: rook
 * 4: queen
 * 5: king
 * 6: (nil)
 */

/* Format of 32-bit supplementary state variable bits:
 *  0- 2: The file on which white double-pushed a pawn last move
 *     3: 1 if white double-pushed a pawn last move
 *  4- 9: position of white king
 *    10: 1 if white king has ever moved
 *    11: 1 if white queenside rook has ever moved
 *    12: 1 if white kingside rook has ever moved
 *
 * The same repeats starting from bit 16 for black.
 */

/* 16-bit encoding of moves:
 *  0- 5: start location
 *  6-11: end location
 * 12-15: depend on the moving piece.
 *
 * if the moving piece is a pawn:
 *  0 - single push, no promotion (= promote to pawn)
 *  1 - single push, promote to knight
 *  2 - single push, promote to bishop
 *  3 - single push, promote to rook
 *  4 - single push, promote to queen
 *  5 - direct capture, no promotion (= promote to pawn)
 *  6 - direct capture, promote to knight
 *  7 - direct capture, promote to bishop
 *  8 - direct capture, promote to rook
 *  9 - direct capture, promote to queen
 * 10 - double push
 * 11 - en passant
 *
 * if the moving piece is the king:
 *  0 - 1 space move
 * 12 - queenside castle
 * 13 - kingside castle
 *
 * 0 for all other moves.
 */

// Which player we are currently evaluating for. Flip-flops during min-max search.
// 0 - white, 1 - black
int me;
uint64_t pcs[2][5];
uint32_t supp;
// One byte per square to quickly look up what kind of piece
// is on every position. Does not track color however.
char brd[64];

struct timeval since;
jmp_buf endjmp;

uint64_t prays[4][64], nrays[4][64];

int
make(uint16_t m)
{
	int s = m & 63,
	    e = m/64 & 63,
	    x = m >> 12,
	    c = e, // capture location
	    p = brd[s], q; // p is moving piece, q is captured piece

	assert(p < 6);
	assert(s != e);

	// clear double pawn push bits
	supp &= ~(15 << 16*me);
	if (x == 10) // set double push
		supp |= (8|(s&7)) << 16*me;

	// remove piece from the square it started on
	if (p < 5)
		UNSET(pcs[me][p], s);
	brd[s] = 6;

	// capture enemy piece
	if (x == 11) // en passant
		c = (s&070)+(e&7);
	q = brd[c];
	if (q < 5)
		UNSET(pcs[!me][q], c);
	brd[c] = 6;

	if (x > 0 && x < 5) // single push promotion
		p = x;
	if (x > 5 && x < 10) // direct capture promotion
		p = x - 5;

	// put the piece down where it ends on
	if (p < 5) {
		SET(pcs[me][p], e);
	} else {
		supp &= ~(63 << 4+16*me);
		supp |= (64|e) << 4+16*me;
	}
	brd[e] = p;

	if (x == 12) // queenside castle
		make((s&070) | (s&070)+3 << 6);
	if (x == 13) // kingside castle
		make((s&070)+7 | (s&070)+5 << 6);

	if (p == 3) { // update rook bits
		if (s == 0 || s == 070)
			supp |= 1 << 11+16*me;
		if (s == 7 || s == 077)
			supp |= 1 << 12+16*me;
	}

	assert(p == 0 ? e/8 != 0 && e/8 != 7 : 1);
	assert(brd[supp >> 4+16*me & 63] == 5);
	
	return q;
}

void
unmake(uint16_t m, int q)
{
	int s = m & 63,
	    e = m/64 & 63,
	    x = m >> 12,
	    c = e, // capture location
	    p = brd[e];

	assert(p < 6);
	assert(s != e);

	// remove piece from the square it ended on
	if (p < 5)
		UNSET(pcs[me][p], e);
	brd[e] = 6;

	// undo promotion
	if (x > 0 && x < 5)
		p = 0;
	if (x > 5 && x < 10)
		p = 0;

	// undo capture
	if (x == 11) // en passant
		c = (s&070)+(e&7);
	if (q < 5)
		SET(pcs[!me][q], c);
	brd[c] = q;

	// put the piece down where it started on
	if (p < 5) {
		SET(pcs[me][p], s);
	} else {
		supp &= ~(63 << 4+16*me);
		supp |= (64|s) << 4+16*me;
	}
	brd[s] = p;

	if (x == 12) // queenside castle
		unmake((s&070) | (s&070)+3 << 6, 6);
	if (x == 13) // kingside castle
		unmake((s&070)+7 | (s&070)+5 << 6, 6);
}

int
pseudo(uint16_t *m)
{
	int n = 0;

	uint64_t mine = pcs[me][0] | pcs[me][1] | pcs[me][2] | pcs[me][3] | pcs[me][4];
	SET(mine, supp >> 4+16*me & 63);
	uint64_t enem = pcs[!me][0] | pcs[!me][1] | pcs[!me][2] | pcs[!me][3] | pcs[!me][4];
	SET(enem, supp >> 4+16*!me & 63);

	// pawns
	int d = me?-8:8;
	for (uint64_t b = pcs[me][0]; b; b &= b-1) {
		int i = __builtin_ffsl(b)-1;
		int p = me ? 7-i/8 : i/8; // progress along the board
		if (brd[i+d]==6) {
			m[n++] = i | i+d<<6 | (p+1==7?4<<12:0);
			if (p == 1 && brd[i+2*d] == 6)
				m[n++] = i | i+2*d<<6 | 10<<12;
		}
		if ((i&7)>0 && ISSET(enem, i+d-1)) {
			m[n++] = i | i+d-1<<6 | 5+(p+1==7?4:0)<<12;
		}
		if ((i&7)<7 && ISSET(enem, i+d+1)) {
			m[n++] = i | i+d+1<<6 | 5+(p+1==7?4:0)<<12;
		}
		// TODO en passant
	}

	// knights
	for (uint64_t b = pcs[me][1]; b; b &= b-1) {
		int i = __builtin_ffsl(b)-1, r=i/8, f=i&7;
		if (r+2<8) {
			if (f+1<8 && !ISSET(mine, i+17))
				m[n++] = i | i+17 << 6;
			if (f-1>=0 && !ISSET(mine, i+15))
				m[n++] = i | i+15 << 6;
		}
		if (r-2>=0) {
			if (f+1<8 && !ISSET(mine, i-15))
				m[n++] = i | i-15 << 6;
			if (f-1>=0 && !ISSET(mine, i-17))
				m[n++] = i | i-17 << 6;
		}
		if (f+2<8) {
			if (r+1<8 && !ISSET(mine, i+10))
				m[n++] = i | i+10 << 6;
			if (r-1>=0 && !ISSET(mine, i-6))
				m[n++] = i | i-6 << 6;
		}
		if (f-2>=0) {
			if (r+1<8 && !ISSET(mine, i+6))
				m[n++] = i | i+6 << 6;
			if (r-1>=0 && !ISSET(mine, i-10))
				m[n++] = i | i-10 << 6;
		}
	}

	// rook & cardinal queen movement
	for (uint64_t b = pcs[me][3] | pcs[me][4]; b; b &= b-1) {
		int d, i = __builtin_ffsl(b)-1, r=i/8, f=i&7;
		for (d = f+1; d<8 && !ISSET(mine, 8*r+d); d++) {
			m[n++] = i | 8*r+d << 6;
			if (ISSET(enem, 8*r+d)) break;
		}
		for (d = f-1; d>=0 && !ISSET(mine, 8*r+d); d--) {
			m[n++] = i | 8*r+d << 6;
			if (ISSET(enem, 8*r+d)) break;
		}
		for (d = r+1; d<8 && !ISSET(mine, 8*d+f); d++) {
			m[n++] = i | 8*d+f << 6;
			if (ISSET(enem, 8*d+f)) break;
		}
		for (d = r-1; d>=0 && !ISSET(mine, 8*d+f); d--) {
			m[n++] = i | 8*d+f << 6;
			if (ISSET(enem, 8*d+f)) break;
		}
	}

	// bishop & diagonal queen movement
	for (uint64_t b = pcs[me][2] | pcs[me][4]; b; b &= b-1) {
		int d, i = __builtin_ffsl(b)-1, r=i/8, f=i&7;
		for (d = 1; r+d<8 && f+d<8 && !ISSET(mine, i+9*d); d++) {
			m[n++] = i | i+9*d << 6;
			if (ISSET(enem, i+9*d)) break;
		}
		for (d = 1; r+d<8 && f-d>=0 && !ISSET(mine, i+7*d); d++) {
			m[n++] = i | i+7*d << 6;
			if (ISSET(enem, i+7*d)) break;
		}
		for (d = 1; r-d>=0 && f+d<8 && !ISSET(mine, i-7*d); d++) {
			m[n++] = i | i-7*d << 6;
			if (ISSET(enem, i-7*d)) break;
		}
		for (d = 1; r-d>=0 && f-d>=0 && !ISSET(mine, i-9*d); d++) {
			m[n++] = i | i-9*d << 6;
			if (ISSET(enem, i-9*d)) break;
		}
	}

	// king movement
	int i = supp >> 4+16*me & 63;
	for (int r = i/8-1; r <= i/8+1; r++) {
		if (r < 0 || r > 7) continue;
		for (int f = (i&7)-1; f <= (i&7)+1; f++) {
			if (f < 0 || f > 7) continue;
			// TODO avoid running into check
			if (!ISSET(mine, 8*r+f)) {
				m[n++] = i | 8*r+f << 6;
			}
		}
	}
	// TODO castling

	return n;
}

int64_t
eval()
{
	int64_t s = 0;

	// materiel value
	int g[] = { 1, 3, 3, 5, 9 };
	for (int p = 0; p < 5; p++)
		s += g[p] * 100 * (__builtin_popcountl(pcs[0][p]) - __builtin_popcountl(pcs[1][p]));

	// pawn progression
	for (int p = 0; p < 7; p++) {
		s += p * 10 * __builtin_popcountl(pcs[0][0] & (0xFFul << 8*p));
		s -= (7-p) * 10 * __builtin_popcountl(pcs[1][0] & (0xFFul << 8*p));
	}

	return me ? -s : s;
}

int64_t maxsearch(int, int64_t, int64_t, uint16_t *);

int64_t
minsearch(int depth, int64_t alpha, int64_t beta)
{
	struct timeval now;
	gettimeofday(&now, 0);
	int64_t d1 = now.tv_sec - since.tv_sec;
	d1 *= 1000;
	int64_t d2 = now.tv_usec - since.tv_usec;
	d2 /= 1000;
	if (d1 + d2 >= 300) longjmp(endjmp, 1);

	if (!depth) return -eval();
	int64_t s;
	uint16_t m[256];
	uint32_t o = supp;
	int n = pseudo(m);
	for (int i = 0; i < n; i++) {
		int c = make(m[i]);
		me ^= 1;
		s = c == 5 ? -GOOD : maxsearch(depth-1, alpha, beta, 0);
		me ^= 1;
		unmake(m[i], c);
		supp = o;
		if (s <= alpha)
			return alpha;
		if (s < beta) {
			beta = s;
		}
	}
	return beta;
}

int64_t
maxsearch(int depth, int64_t alpha, int64_t beta, uint16_t *bm)
{
	struct timeval now;
	gettimeofday(&now, 0);
	int64_t d1 = now.tv_sec - since.tv_sec;
	d1 *= 1000;
	int64_t d2 = now.tv_usec - since.tv_usec;
	d2 /= 1000;
	if (d1 + d2 >= 300) longjmp(endjmp, 1);

	if (!depth) return eval();
	int64_t s;
	uint16_t m[256];
	uint32_t o = supp;
	int n = pseudo(m);
	for (int i = 0; i < n; i++) {
		int c = make(m[i]);
		me ^= 1;
		s = c == 5 ? GOOD : minsearch(depth-1, alpha, beta);
		me ^= 1;
		unmake(m[i], c);
		supp = o;
		if (s >= beta)
			return beta;
		if (s > alpha) {
			alpha = s;
			if (bm) *bm = m[i];
		}
	}
	return alpha;
}

void
go()
{
	uint16_t m = 0;
	if (!setjmp(endjmp)) {
		gettimeofday(&since, 0);
		for (int depth = 1; depth < 30; depth++) {
			printf("info depth %d\n", depth);
			maxsearch(depth, -GOOD, GOOD, &m);
		}
	}
	if (m) {
		printf("bestmove %c%c%c%c",
			(m&7)+'a', (m/8&7)+'1',
			(m>>6&7)+'a', ((m>>6)/8&7)+'1');
		int x = m >> 12;
		if (x > 0 && x < 5)
			putchar("nbrq"[x-1]);
		if (x > 5 && x < 10)
			putchar("nbrq"[x-6]);
		putchar('\n');
	} else {
		puts("bestmove 0000");
	}
}

#if FULL
void
show()
{
	puts("           (black)");
	for (int r = 8; r--;) {
		printf(" %d ", r+1);
		for (int f = 0; f < 8; f++) {
			if (r+f&1)
				printf("\033[30;47m");
			else
				printf("\033[30;46m");
			unsigned i = 8*r+f;
			char p = brd[i];
			char *wg[] = { "\u2659","\u2658","\u2657","\u2656","\u2655","\u2654" };
			char *bg[] = { "\u265f","\u265e","\u265d","\u265c","\u265b","\u265a" };
			const char *sel = " ";
			if (p < 5) {
				if (ISSET(pcs[0][p], i)) {
					sel = wg[p];
				} else {
					sel = bg[p];
				}
			} else if (p < 6) {
				if (i == (supp >> 4 & 63)) {
					sel = wg[5];
				} else {
					sel = bg[5];
				}
			}
			printf(" %s ", sel);
		}
		printf("\033[39;49m\n");
	}
	printf("    a  b  c  d  e  f  g  h\n");
	puts("           (white)");
}

int
islegal()
{
	uint32_t o = supp;
	uint16_t m[265];
	for (int n = pseudo(m); n--;) {
		int c = make(m[n]);
		unmake(m[n], c);
		supp = o;
		if (c == 5) return 0;
	}
	return 1;
}

uint64_t
perft(int depth)
{
	if (!depth) return 1;
	uint64_t p = 0;
	uint32_t o = supp;
	uint16_t m[256];
	for (int n = pseudo(m); n--;) {
		int c = make(m[n]);
		me ^= 1;
		if (islegal())
			p += perft(depth-1);
		me ^= 1;
		unmake(m[n], c);
		supp = o;
	}
	return p;
}
#endif

int
main()
{
	// initialize rays
	for (int i = 0; i < 64; i++) {
		for (int l = 1; l < 8; l++) {
			if ((i&7)+l<8) SET(prays[0][i], i+l);
			if (i/8+l<8) {
				SET(prays[1][i], i+8*l);
				if ((i&7)+l<8) SET(prays[2][i], i+9*l);
				if ((i&7)-l>=0) SET(prays[3][i], i+7*l);
			}

			if ((i&7)-l>=0) SET(nrays[0][i], i-l);
			if (i/8-l>=0) {
				SET(nrays[1][i], i-8*l);
				if ((i&7)+l<8) SET(nrays[2][i], i-7*l);
				if ((i&7)-l>=0) SET(nrays[3][i], i-9*l);
			}
		}
	}

	char l[999], *t;
	while (fgets(l, 999, stdin)) {
		t = strtok(l, " \t\n");
		if (!t) continue;
		if (!strcmp(t, "go")) {
			go();
		} else if (!strcmp(t, "isready")) {
			puts("readyok");
		} else if (!strcmp(t, "position")) {
			strtok(0, " \t\n");
			strtok(0, " \t\n");

			// reset the board to the starting position
			me = 0;
			supp = 0x3c00040;
			pcs[0][0] = 0xFF00;
			pcs[0][1] = 0x42;
			pcs[0][2] = 0x24;
			pcs[0][3] = 0x81;
			pcs[0][4] = 8;
			pcs[1][0] = 0xFFll<<48;
			pcs[1][1] = 0x42ll<<56;
			pcs[1][2] = 0x24ll<<56;
			pcs[1][3] = 0x81ll<<56;
			pcs[1][4] = 8ll<<56;

			for (int p, i = 0; i < 64; i++) {
				for (p = 0; p < 5 && !ISSET(pcs[0][p] | pcs[1][p], i); p++);
				brd[i] = p < 5 ? p : 6;
			}
			brd[4] = brd[4+7*8] = 5;
			
			while ((t = strtok(0, " \t\n"))) {
				int s = t[0]-'a' | 8*(t[1]-'1'),
				    e = t[2]-'a' | 8*(t[3]-'1'),
				    x = 0;
				char p = brd[s];
				if (!p) { // discern what kind of move a pawn is making
					int d = ABS(e - s);
					if (d == 16) // double push
						x = 10;
					else if (d == 7 || d == 9) // capture
						x = brd[e] < 6 ? 5 : 11;
					int g[256] = {['b']=1,['n']=2,['r']=3,['q']=4};
					x += g[t[4]];
				} else if (p == 5) { // detect castling
					if (e == s - 2) // queenside
						x = 12;
					else if (e == s + 2) // kingside
						x = 13;
				}
				make(s | e<<6 | x<<12);
				me ^= 1;
			}
#if FULL
		} else if (!strcmp(t, "show")) {
			show();
		} else if (!strcmp(t, "perft")) {
			t = strtok(0, " \t\n");
			uint64_t total = perft(*t - '0');
			printf("perftresult %lu\n", total);
#endif
		} else if (!strcmp(t, "uci")) {
			puts("id name bb_pawneval\nid author Thomas Oltmann\nuciok");
		} else if (!strcmp(t, "quit")) {
			return 0;
		}
		fflush(stdout);
	}
}
