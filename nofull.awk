# pre-preprocess sources to get rid of disabled code before compilation
# BUG doesn't know about backslash-escaped newlines
BEGIN {
	depth = 0;
}
/^[ \t]*#/ {
	$0 = substr($0, index($0, "#") + 1);
	if ($1 ~ /^(if|ifdef)$/) {
		if ((NF == 2 && $2 ~ /^FULL$/) || depth) {
			depth++;
		}
	} else if ($1 ~ /^ifndef$/) {
		if (depth) {
			depth++;
		}
	} else if ($1 ~ /^endif$/) {
		if (depth) {
			depth--;
			next;
		}
	}
	$0 = "#" $0;
}
{
	if (!depth) {
		print $0;
	}
}
