# BUG will insert unneccessary newlines around preprocessor directives inside block comments
# BUG doesn't know about backslash-escaped newlines
BEGIN {
	# modes:
	# 0 - normal C or preprocessor code
	# 1 - inside string literal
	# 2 - inside block comment
	# 3 - inside line comment
	mode = 0;
	# was the currently read token preceded by whitespace?
	spaced = 0;
	# was a line break emitted before this line?
	breaked = 1;
	# did the previously outputted token end on a letter or number?
	endalpha = 0;
}
{
	# start preprocessor directives on new lines
	if ($0 ~ /^[ \t]*#/ && !breaked) {
		printf "\n";
		endalpha = 0;
		breaked = 1;
	}

	linelen = length($0);
	for (i = 1; i <= linelen; i++) {
		suf = substr($0, i);
		chr = substr(suf, 1, 1);

		if (mode == 0) { # normal code
			if (suf ~ /^[ \t\n]/) { # skip whitespace, but take note of it
				spaced = 1;
			} else if (suf ~ /^"/) { # enter string literal
				printf "%s", chr;
				mode = 1;
				endalpha = 0;
				breaked = 0;
			} else if (suf ~ /^\/\*/) { # enter block comment
				mode = 2;
				i++;
			} else if (suf ~ /^\/\//) { # enter line comment
				mode = 3;
				i++;
			} else if (suf ~ /^[A-Za-z0-9_]/) { # alphanumeric character
				# preserve spaces between alphanumeric tokens
				if (spaced && endalpha) {
					printf " ";
				}
				printf "%s", chr;
				spaced = 0;
				endalpha = 1;
				breaked = 0;
			} else { # symbol character
				printf "%s", chr;
				spaced = 0;
				endalpha = 0;
				breaked = 0;
			}
		} else if (mode == 1) { # string literal
			if (suf ~ /^\\[\\"]/) { # don't get confused by escapes
				printf "%s", substr(suf, 1, 2);
				i++;
			} else if (suf ~ /^"/) { # exit string literal
				printf "%s", chr;
				mode = 0;
			} else {
				printf "%s", chr;
			}
		} else if (mode == 2) { # block comment
			if (suf ~ /^\*\//) {
				i++;
				mode = 0;
			}
		}
		# line comments simply munch everything left
	}

	# terminate preprocessor lines with explicit newline
	if ($0 ~ /^[ \t]*#/) {
		printf "\n";
		endalpha = 0;
		breaked = 1;
	}

	# turn over to next line
	spaced = 1;
	if (mode == 1 || mode == 3) {
		mode = 0;
	}
}
