CC=gcc
#CFLAGS=-g -std=c11 -Wall -Wextra -pedantic -Wno-parentheses -Wno-char-subscripts -fsanitize=address -O2 -march=native
CFLAGS=-g -std=c11 -Wall -Wextra -pedantic -Wno-parentheses -Wno-char-subscripts -O2 -march=native
CPPFLAGS=-DFULL=1 -D_GNU_SOURCE

.PHONY: all clean

all: blunderbuss blunderbuss4k bitboarded

clean:
	rm -f blunderbuss
	rm -f blunderbuss4k.c
	rm -f blunderbuss4k
	rm -f bitboarded

blunderbuss: blunderbuss.c
	$(CC) $(CFLAGS) -o $@ $^ $(CPPFLAGS)

blunderbuss4k: blunderbuss4k.c loader.sh
	cp loader.sh $@
	xz --stdout blunderbuss4k.c >> $@
	chmod 755 $@
	wc -c blunderbuss4k

blunderbuss4k.c: blunderbuss.c nofull.awk squish.awk
	awk -f nofull.awk $< | awk -f squish.awk > $@

bitboarded: bitboarded.c
	$(CC) $(CFLAGS) -o $@ $^ $(CPPFLAGS)
